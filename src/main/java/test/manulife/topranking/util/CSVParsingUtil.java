package test.manulife.topranking.util;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

import org.springframework.core.io.ClassPathResource;

import test.manulife.topranking.model.WebSite;

public class CSVParsingUtil {
	
	public static final String DEFAULT_CSV_VALUE_SEPARATOR = "|"; 
	public static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd";
	
	public static List<WebSite> parseCSV(String pathToFile){
		return parseCSV(pathToFile, DEFAULT_CSV_VALUE_SEPARATOR);
	}
	
	public static List<WebSite> parseCSV(String pathToFile, String valueSeparator){
		List<WebSite> webSites = new ArrayList<WebSite>();
		Scanner scanner = null;
		try{
			File csvFile = new ClassPathResource(pathToFile).getFile();
			scanner = new Scanner(csvFile);
			//bypassing header line
			scanner.nextLine();
			while (scanner.hasNext()) {
				WebSite webSite = parseLine(scanner.nextLine(), valueSeparator);
				if(webSite != null){
					webSites.add(webSite);
				}
			}
		}catch (IOException | ParseException ex ) {
			int i =0;
			// TODO: handle exception
		}finally{
			if(scanner != null){
				scanner.close();
			}
		}

		return webSites;
	}
	
	private static WebSite parseLine(String line, String valueSeparator) throws ParseException{
		String separator = (valueSeparator != null && valueSeparator.length() > 0)?valueSeparator:DEFAULT_CSV_VALUE_SEPARATOR;
		String[] webSiteValues = line.split(Pattern.quote(separator));
		WebSite webSite = null;
		if(webSiteValues != null && webSiteValues.length == 3){
			webSite = new WebSite();
			// TODO validate values after parsing
			SimpleDateFormat formatter = new SimpleDateFormat(DEFAULT_DATE_FORMAT);
			Date rankDate = formatter.parse(webSiteValues[0]);
			webSite.setRankDate(rankDate);
			webSite.setName(webSiteValues[1]);
			webSite.setNumberOfVisit(Integer.valueOf(webSiteValues[2]));
		}
		return webSite;
	}
}
