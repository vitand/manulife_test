package test.manulife.topranking.model;

import java.util.Date;

public class WebSite {
	
	private Date rankDate;
	private String name;
	private Integer numberOfVisit; //depending on numbers BigInteger migth be considered
	
	public Date getRankDate() {
		return rankDate;
	}
	public void setRankDate(Date rankDate) {
		this.rankDate = rankDate;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getNumberOfVisit() {
		return numberOfVisit;
	}
	public void setNumberOfVisit(Integer numberOfVisit) {
		this.numberOfVisit = numberOfVisit;
	}
}
