package test.manulife.topranking.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import test.manulife.topranking.dao.ITopRankingDAO;
import test.manulife.topranking.model.WebSite;
import test.manulife.topranking.service.ITopRankingService;

@Service
public class ToRankingServiceImpl implements ITopRankingService {
	
	@Autowired
	ITopRankingDAO topRankingDAO;

	@Override
	public List<WebSite> getTopRatedWebSitesByDate(Date date, Integer numberOfTop) {
		return topRankingDAO.getTopRatedWebSitesByDate(date, numberOfTop);
	}

}
