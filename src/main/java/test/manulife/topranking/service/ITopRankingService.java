package test.manulife.topranking.service;

import java.util.Date;
import java.util.List;

import test.manulife.topranking.model.WebSite;

public interface ITopRankingService {
	
	public List<WebSite> getTopRatedWebSitesByDate(Date date, Integer numberOfTop);

}
