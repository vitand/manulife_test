package test.manulife.topranking.dao.impl;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;

import test.manulife.topranking.dao.ITopRankingDAO;
import test.manulife.topranking.model.WebSite;
import test.manulife.topranking.util.CSVParsingUtil;

@Repository
public class TopRankingDAO implements ITopRankingDAO {
	
	private final String PATH_TO_CSV_FILE = "data/data.csv";
	
	private List<WebSite> webSiteListCache;
	
	@Override
	public List<WebSite> getTopRatedWebSitesByDate(Date date, Integer numberOfTop) {
		List<WebSite> webSites = null;
		if(webSiteListCache != null){
			webSites = webSiteListCache;
		}else{
			webSites = CSVParsingUtil.parseCSV(PATH_TO_CSV_FILE);
		}
		return webSites.stream()
				.filter(ws -> date.equals(ws.getRankDate()))
				.sorted((ws1, ws2) -> Integer.compare(ws2.getNumberOfVisit(), ws1.getNumberOfVisit()))
				.limit(numberOfTop)
				.collect(Collectors.toList());
	}
}
