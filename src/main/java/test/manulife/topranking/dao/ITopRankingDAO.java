package test.manulife.topranking.dao;

import java.util.Date;
import java.util.List;

import test.manulife.topranking.model.WebSite;

public interface ITopRankingDAO {
	
	List<WebSite> getTopRatedWebSitesByDate(Date date, Integer numberOfTop);

}
