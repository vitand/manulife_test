package test.manulife.topranking;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

@SpringBootApplication(scanBasePackages={"test.manulife.topranking"})
public class TopRankingSpringBoot extends SpringBootServletInitializer {
    public static void main(String[] args) {
        SpringApplication.run(TopRankingSpringBoot.class, args);
	}
    
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(TopRankingSpringBoot.class);
    }
}
