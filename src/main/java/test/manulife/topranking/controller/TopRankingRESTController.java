package test.manulife.topranking.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import test.manulife.topranking.model.WebSite;
import test.manulife.topranking.service.ITopRankingService;

@RestController
@RequestMapping("/rest/rank")
public class TopRankingRESTController {
	
	@Autowired
	ITopRankingService topRankingService;
	
	@RequestMapping(method=RequestMethod.GET)
	public int getWebSiteByName(){
		throw new UnsupportedOperationException();
	}
	
	@RequestMapping(value="/filter", method=RequestMethod.GET)
	public List<WebSite> getTopRatedWebSitesByDate(
										@RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") Date date, 
										@RequestParam("top") Integer numberOfTop){
		return topRankingService.getTopRatedWebSitesByDate(date, numberOfTop);
	}
}
