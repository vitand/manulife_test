var app = angular.module('topRanking', ['ngRoute', 'ngResource']);

app.config(function($routeProvider){
	$routeProvider
   .when('/ranking',{
	   templateUrl: './view/ranking.html',
	   controller: 'topRankingController'
   })
   .otherwise(
		   { redirectTo: '/ranking'}
   );
});

app

app.controller('topRankingController', function($scope, $filter, $http, $location) {
	$scope.$watch('date', function(newVal, oldVal){
	    var filteredDate = $filter('date')(newVal, 'yyyy-MM-dd');
	    if(filteredDate != null){
	    // can be done through AngularJS resource but since there is only one method to call ...  
	    var url = "./rest/rank/filter?date=" + filteredDate+"&top=5";
	    $scope.date1 = url;
	    $http.get(url).success( function(response) {
	        $scope.webSites = response; 
	    });
	}
	});
});
